export interface Book {
    id: string;
    name: string;
    authors: string[]
    year: number,
    price: number
}