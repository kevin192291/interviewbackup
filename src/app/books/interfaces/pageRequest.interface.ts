export interface PageRequest {
  page: number;
  size: number;
  sortColumn: string;
  sortDirection: string;
}
