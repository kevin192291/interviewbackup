import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from '../interfaces/book.interface';
import { PageRequest } from '../interfaces/pageRequest.interface';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  @Input() model: Book[];
  @Input() atEnd: boolean;
  @Input() pageCount: number;
  @Input() lastPage: number;
  @Output() public newPage: EventEmitter<PageRequest> = new EventEmitter<
    PageRequest
  >();
  @Output() public selection: EventEmitter<number> = new EventEmitter<number>();
  @Output() public sorted: EventEmitter<string> = new EventEmitter<string>();
  @Output() public selected: EventEmitter<string[]> = new EventEmitter<
    string[]
  >();
  public pageNumber: number = 0;
  public pageSize: number = 10;
  public sortColumn: string = '';
  private selectedRows: string[] = [];
  private sortDirection: string = null;

  constructor() {}

  ngOnInit() {}

  sort(column: string) {
    this.sortColumn = column;
    if (!this.sortDirection) {
      this.sortDirection = 'desc';
    } else if (this.sortDirection === 'desc') {
      this.sortDirection = 'asc';
    } else if (this.sortDirection === 'asc') {
      this.sortDirection = null;
    }
    this.changePage(this.pageNumber);
  }

  public changePage(pageNum: number) {
    const num =
      pageNum < 0 ? 0 : pageNum >= this.lastPage ? this.lastPage - 1 : pageNum;
    this.pageNumber = num;
    this.newPage.emit({
      page: num,
      size: Number(this.pageSize),
      sortColumn: this.sortColumn,
      sortDirection: this.sortDirection
    });
  }

  onSizeChange(selectedPageSize: number) {
    this.pageSize = selectedPageSize;
    this.selection.emit(selectedPageSize + this.pageNumber * this.pageSize);
  }

  onRowSelected($event, id: string) {
    if (!this.selectedRows.includes(id) && $event.target.checked) {
      this.selectedRows.push(id);
    }
    if (this.selectedRows.includes(id) && !$event.target.checked) {
      const index = this.selectedRows.indexOf(id);
      this.selectedRows.splice(index, 1);
    }
    this.selected.emit(this.selectedRows);
  }
}
