import { Injectable } from '@angular/core';
import { Book } from '../books/interfaces/book.interface';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private _books: Book[] = [];
  private _dataLength: number = 1005;
  public books: Book[] = [];
  public reachedEnd: boolean = false;
  public pageCount = 0;

  constructor() {
    for (let index = 0; index < this._dataLength; index++) {
      const authors: string[] = [];

      for (let index = 0; index < Math.floor(Math.random() * 5); index++) {
        authors.push(this.makeStr(10));
      }

      this._books.push({
        id: this.generateId(),
        name: this.makeStr(6),
        authors: authors,
        year: 1800 + index,
        price: Math.floor(Math.random() * 200)
      });
    }
  }

  public getBookById(ids: string[]): Book[] {
    const books: Book[] = [];
    ids.forEach(id => {
      books.push(this._books.find(book => book.id === id));
    });
    return books;
  }

  public pageData(
    page: number = 0,
    size: number = 10,
    sortColumn = null,
    sortDirection: string = null,
  ): Book[] {
    const skip = page * size;
    const take = skip + size;
    this.pageCount = Math.ceil(this._dataLength / size);
    if (skip + take >= this._dataLength) {
      this.reachedEnd = true;
    } else {
      this.reachedEnd = false;
    }

    if (sortColumn) {
      const sorted = (this.books = this._books
        .sort(function(a, b) {
          if (a[sortColumn] < b[sortColumn]) {
            return -1;
          }
          if (a[sortColumn] > b[sortColumn]) {
            return 1;
          }
          return 0;
        }));
      if (sortDirection === 'asc') {
        return sorted.reverse().slice(skip, take);
      } else {
        return sorted.slice(skip, take);
      }
    }
    return (this.books = this._books.slice(skip, take));
  }

  //Lets create some Mock Ids and Names:
  private generateId() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (Math.random() * 16) | 0,
        v = c == 'x' ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  }

  private makeStr(length) {
    var text = '';
    var possible = 'abcdefghijklmnopqrstuvwxyz';

    for (var i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
}
