import { Component } from '@angular/core';
import { DataService } from './services/data.service';
import { PageRequest } from './books/interfaces/pageRequest.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'interviewTest';

  constructor(public data: DataService) {
    data.pageData();
  }

  pageChanged($event: PageRequest) {
    this.data.pageData($event.page, $event.size, $event.sortColumn, $event.sortDirection);
  }

  onSelection($event) {
    const book = this.data.getBookById($event);
    console.log('Selected Book: ', book);
  }
}
